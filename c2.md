## De los ejercicios de la sección A y B:

 - ¿Que powerup(mejora) desarrollarías y por que?.
	- Contador de vocales, en este ejercicio hubiera implementado una validación para que al sustituir la vocal también respetara si se encontraba en mayúsculas. ¿Porque? para respetar el formato del texto original.
	- Data set
		- Haber implementado alguna librería como pyfunctional, al ser requisito del ejercicio no aplicar librerías o funciones nativas. Tuve que desarrollar un código muy extenso por lo cual con el uso de alguna librería quizás hubiera hecho menos código. También la posibilidad de poder captura ya sea por consola o mediante algún consumo de API (backend) el data set a evaluar. ¿Porque? con el fin de hacerlo mas escalable.


### Ejecuta un analizador de sintaxis Flake8 y el analizador de complejidad, del
resultado obtenido:
Flake8, me permitió evaluar el código y adecuar el estilo a la convención de PEP8 por ejemplo me permitió quitar espacios, líneas de código que pasaban las 79 caracteres, librerías u objetos no utilizados.

El estándar utilizado para la evaluación de la sintaxis fue PEP8.