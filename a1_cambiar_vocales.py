vocales = ('a', 'e', 'i', 'o', 'u')


class AnalizarVocales:

    def __init__(self, texto: str):
        self.texto = texto

    def contar_vocales(self) -> int:
        contador: int = 0
        text = self.texto
        for letra in text:
            if letra.lower() in vocales:
                contador += 1
        return contador

    def cambiar_vocales(self) -> str:
        text: str = ''

        for letra in self.texto:
            if letra.lower() in vocales:
                ind = vocales.index(letra)
                # si se encuentra la vocal u agrega la a para evitar truene
                reemplazo = vocales[ind + 1] if letra != 'u' else vocales[0]
                text += reemplazo
            else:
                text += letra

        return text


if __name__ == '__main__':
    entrada = input("Ingresa el texto o parráfo a analizar \n")
    analizador_vocales = AnalizarVocales(entrada)
    total_vocales: int = analizador_vocales.contar_vocales()
    print(f'El parrafo o frase {entrada} tiene {total_vocales} vocales')
    texto_vocales_cambiadas = analizador_vocales.cambiar_vocales()
    print(texto_vocales_cambiadas)
