## Definición de pruebas
Basado en la siguiente HU (Historia de usuario), define las pruebas a realizar:
● Como Cliente, quiero suscribirme a un canal Premium por períodos flexibles de tiempo por medio del sitio web.

 1. Determinar los campos mínimos del cliente, que requiere la suscripción como correo, nombre, apellido, edad, contraseña, etc.
 2. Como Validar la identidad del cliente
 3. Determinar el método de pago que aceptara el sitio web y la manera de proteger la información de pago.
 4. Determinar la seguridad por para del sitio web.
 5. Determinar la manera en que se cumple con Ley de Protección de Datos.
 6. Si es necesario un mínimo de días para los períodos flexibles.
 7. Determinar el alcance geográfico del sitio web.
 8. Determinar las APIS requeridas por el fronted.
 9. Determinar el tipo de despliegue.
 10. Definir el comportamiento del sitio web cuando se le ingresa información incompleta o corrupta. 
 11. Verificar que las APIS están retornando los status code y la estructura del json correcto.
 12. Someter al sitio web a sobrecarga e inyección sql.
 