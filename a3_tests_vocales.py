import unittest
from a1_cambiar_vocales import AnalizarVocales


class AnalizarVocalesTest(unittest.TestCase):
    def test_analizador_vocales(self):
        analizador_vocales = AnalizarVocales('hacer')
        total_vocales: int = analizador_vocales.contar_vocales()
        self.assertIsInstance(total_vocales, int)
        self.assertEqual(total_vocales, 2)
        texto_cambio_vocales: str = analizador_vocales.cambiar_vocales()
        self.assertIsInstance(texto_cambio_vocales, str)
        self.assertEqual(texto_cambio_vocales, 'hecir')


if __name__ == '__main__':
    unittest.main()
