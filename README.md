## Envio Challenge

Este ejemplo fue construido bajo python 3.8

  

### Pasos para ejecutar
1. Descargar este repositorio `git clone https://gitlab.com/garache/envio-prueba.git`
2. Posicionarse en raíz del proyecto recién descargado:
Crear el entorno virtual
`virtualenv RUTA_DEL_ENTORNO_VIRTUAL/NOMBRE_ENTORNO`
Instalar las dependecias
`pip install -r requirements.txt`
### Configuración
Crear archivo settings.json, en raiz del proyecto. Este archivo no se versiona por buenas prácticas pues contiene información sensible (cadena de conexión), trabaja igual que los archivos .env. Tomar el archivo settings-sample como guia.

>     {
>     "URI_BATABSE": 	""
>     }
Al trabajar con Postgres como motor de base de datos, el valor de URI_DATABASE es una cadena de conexión formateada para dicho motor
`postgresql+psycopg2://{usuario}:{contraseña}@{ip_servidor}:{puerto}/flotillas`
Sustituir los valores por los que tu necesitas.
Para ejecutar el ejercicio de inciso **a1**:
`python .\a1_cambiar_vocales.py`

El programa por terminal pedirá una frase o párrafo al capturarla damos enter e imprimirá el número de vocales que contiene el texto que capturamos y de nuevo el texto pero cambiamos las vocales con la consecutiva siguiente.

3. Para ejecutar el ejercicio del inciso **a2**:

`python .\a2_dataset\main.py`

Esta archivo tomara la estructura dentro de input.py y mostrara un mensaje resumiendo el ordenamiento comparar el resultado de **main.py** con **output_example.txt**.

4. Para ejecutar las pruebas contempladas en el inciso **a3**.

`python .\a3_tests_vocales.py`

Este script ejecuta una batería de pruebas sobre las funciones del **a1_cambiar_vocales.py** que son el resultado que se retorna y la cantidad de caracteres que devuelve la función.

`python .\a2_dataset\a3_tes_dataset.py`

Este script ejecuta una batería de pruebas sobre las funciones del .\a2_dataset\main.py dichas funciones validan si el contenido es del data set es de tipo dict, si contiene el hijo una vez que se ha agregado y el mensaje que retorna.

5. En la carpeta **b2-schema** se localiza el esquema entidad realación y un archivo backup para montar la base de datos en postgres.

6. En el archivo b2_orm_flotilla están los modelos completados en el inciso b2_orm_flotilla y en la carpeta alembic las migraciones.

7. En el archivo **b3-mejoras.md** , estan las mejoras que los incisos **b2** y **b3**.

8. En el archivo c1-pruebas.md se encuentran las pruebas que considera para el inciso **C1**.

  

Thanks for reading.