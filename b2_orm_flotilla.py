from ast import Pass
import os
import json
from datetime import datetime
from socket import IPV6_UNICAST_HOPS
from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    String,
    Boolean,
    exc,
    cast,
    between
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DB_URL: str = ''
path = os.path.abspath(os.path.join('settings.json'))
with open(path) as json_file:
    data = json.load(json_file)

    DB_URL = data['URI_BATABSE']

engine = create_engine(DB_URL)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()


class ModelBase():

    def save(self, **kwargs):
        try:
            session.add(self)
            session.commit()
            response = self.to_json()
            return (True, response)
        except exc.SQLAlchemyError as e:
            print(f'Ha ocurrido un error {e}')
            session.rollback()
            return (False, None)

    def __get_value(self, key):
        if 'fecha' in key or 'hora' in key:
            return str(getattr(self, key))
        else:
            return getattr(self, key)

    def to_json(self):
        from sqlalchemy import inspect
        return {c.key:  self.__get_value(c.key)
                for c in inspect(self).mapper.column_attrs}

    def delete(self, resource):
        session.delete(resource)
        return session.commit()


class Vehicle(ModelBase, Base):
    __tablename__ = 'vehiculos'

    vehiculo_id = Column(Integer, primary_key=True)
    modelo = Column(String, nullable=False)
    marca = Column(String, nullable=False)

    def __init__(self, modelo, marca):
        self.modelo = modelo
        self.marca = marca


class Chofer(ModelBase, Base):
    __tablename__ = 'choferes'
    chofer_id = Column(Integer, primary_key=True)
    chofer = Column(String, nullable=False)

    def __init__(self, chofer):
        self.chofer = chofer


class Asignacion(ModelBase, Base):
    __tablename__ = 'asignaciones'
    asignacion_id = Column(Integer, primary_key=True)
    vehiculo_id = Column(Integer, nullable=False)
    chofer_id = Column(Integer, nullable=False)
    fecha_inicio = Column(String(10), nullable=False)
    fecha_expiracion = Column(String(10), nullable=False)
    vehiculo_manejando = Column(Boolean)

    def __init__(self, vehiculo_id, chofer_id, fecha_inicio,
                 fecha_expiracion, vehiculo_manejando=False):
        self.vehiculo_id = vehiculo_id
        self.chofer_id = chofer_id
        self.fecha_inicio = fecha_inicio
        self.fecha_expiracion = fecha_expiracion
        self.vehiculo_manejando = vehiculo_manejando


def vehiculo_ya_asignado(vehiculo_id: int, chofer_id: int,
                         fecha_inicio: str, fecha_expiracion: str) -> bool:
    vehiculo_ya_asignado = session.query(
            Asignacion.vehiculo_id,
            Asignacion.asignacion_id,
            cast(Asignacion.fecha_inicio, String).label('fecha_inicio'),
            cast(Asignacion.fecha_expiracion, String).label('fecha_expiraion'),
        )\
        .filter(
            Asignacion.vehiculo_id == vehiculo_id,
            Asignacion.chofer_id == chofer_id,
            Asignacion.fecha_inicio >= fecha_inicio,
            Asignacion.fecha_expiracion <= fecha_expiracion
        ) \
        .first()
    return True if vehiculo_ya_asignado else False


def vehiculo_manejandose(chofer_id: int, fecha_inicio: str,
                         fecha_expiracion: str) -> bool:
    vehiculos_manejandose = session.query(
            Asignacion.vehiculo_id
        ) \
        .filter(
            Asignacion.fecha_inicio >= fecha_inicio,
            Asignacion.fecha_expiracion <= fecha_expiracion,
            Asignacion.vehiculo_manejando == bool(1),
            Asignacion.chofer_id == chofer_id
        )
    vehiculos_manejandose = vehiculos_manejandose.first()
    return True if vehiculos_manejandose else False


def asignar_chofer_vehiculo(chofer_id: int, vehiculo_id: int,
                            fecha_inicio: str, fecha_expiracion: str,
                            vehicula_manejando: bool):
    fecha_actual = datetime.today().strftime('%Y-%m-%d')
    asignacion = session.query(
            Vehicle.vehiculo_id,
            Asignacion.asignacion_id,
            cast(Asignacion.fecha_inicio, String).label('fecha_inicio'),
            cast(Asignacion.fecha_expiracion, String).label('fecha_expiraion'),
            Asignacion.vehiculo_manejando
        )\
        .select_from(Vehicle) \
        .join(Asignacion, Asignacion.asignacion_id == Vehicle.vehiculo_id) \
        .filter(
            Asignacion.vehiculo_id == vehiculo_id,
            between(
                fecha_actual, Asignacion.fecha_inicio,
                Asignacion.fecha_expiracion
            )
        ) \
        .first()
    if not asignacion:
        new_asignacion = Asignacion(vehiculo_id, chofer_id, fecha_inicio,
                                    fecha_expiracion, vehicula_manejando)
    else:
        # validar
        asignacion_obj = asignacion._asdict()
        if asignacion['fecha_inicio'] == fecha_actual or \
            asignacion['fecha_expiraion'] == fecha_actual:
            print('Ya existe un registro con esa fecha')
            return
        if asignacion['vehiculo_manejando'] and vehicula_manejando:
            print('Ya existe un vehiculo marcado manejandose')
            return
        new_asignacion = Asignacion(vehiculo_id, chofer_id, fecha_inicio,
                                    fecha_expiracion, vehicula_manejando)
    resp = new_asignacion.save()
    if not resp[0]:
        print('Ocurrio un error al guardar la asignacion')
        return
    print('Se ha guardado la asignación')
    print(resp[1])


def create_tables():
    Base.metadata.create_all(engine)


def menu():
    rigth_entrance: bool = False
    print('Que acción deseas realizar')
    print('1 - Crear base de datos')
    print('2 - Registrar un chofer')
    print('3 - Registrar un vehiculo')
    print('4 - Registrar una asigancion')
    option = input()
    if option == '1':
        create_tables()
    elif  option == '2':
        print('Agrega el nombre del chofer')
        nombre_chofer = input()
        new_chofer = Chofer(chofer=nombre_chofer)
        exito = new_chofer.save()
        if exito[0]:
            print('Chofer guardado con exito')
            print(exito[1])
        else:
            print('Error al registrar chofer')
    elif option == '3':
        print('Agrega el modelo del vehiculo, por ejemplo Fiesta 2016')
        modelo = input()
        print('Agrega la marca por ejemplo Ford')
        marca = input()
        new_vehicle = Vehicle(modelo=modelo, marca=marca)
        exito = new_vehicle.save()
        if exito[0]:
            print('Vehiculo guardado con exito')
            print(exito[1])
        else:
            print('Error al registrar vehiculo')
    elif option == '4':
        print('registrar asignación')
        print('Agrega el id del chofer')
        chofer_id = input()
        print('Agrega el id del vehiculo')
        vehiculo_id = input()
        print('Fecha inicio yyyy-MM-dd por ejemplo 2022-05-25')
        fecha_inicio = input()
        print('Fecha expiracion yyyy-MM-dd por ejemplo 2022-05-25')
        fecha_expiracion = input()
        print('0 no se esta manejando, 1 se esta manejando')
        esta_manejandose = int(input())

        args = {
            'vehiculo_id': vehiculo_id,
            'chofer_id': chofer_id,
            'fecha_inicio': fecha_inicio,
            'fecha_expiracion': fecha_expiracion,
            'vehiculo_manejando': bool(esta_manejandose)
        }
        if vehiculo_ya_asignado(vehiculo_id, chofer_id,
                                fecha_inicio, fecha_expiracion):
            print('Lo sentimos el vehicula ya esta asignado para esas fechas')
            return
        if esta_manejandose and vehiculo_manejandose(chofer_id,
                                                    fecha_inicio,
                                                    fecha_expiracion):
            print('Lo sentimos ya chofer ya esta manejando un vehiculo')
            return
        new_asignacion = Asignacion(**args)
        exito = new_asignacion.save()
        if exito[0]:
            print('Asignacion guardada con exito')
            print(exito[1])
        else:
            print('Error al registrar chofer')
    else:
        print(f'La opción {option} no es válida')


if __name__ == '__main__':
    menu()
