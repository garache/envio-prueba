import unittest
from input import data
from main import DataSet

output = '''
------------Data------------

> One nameA
-----> One nameSubDataA2
----------> One nameSubdataA2D
----------> One nameSubdataA2B
----------> One nameSubdataA2A
-----> One nameSubdataA
> One nameD
> One nameC
-----> One nameSubDataC
> One nameB
-----> One nameSubDataB

------------Data------------

'''
data_e = {
    "DataE":{
        "name": "One nameE",
        "level": "Four",
        "priority": "Lowest",
    }
}


class DataSetTest(unittest.TestCase):

    def test_data_set(self):

        data_set = DataSet(data)
        self.assertIsInstance(data_set.data, dict)
        data_copy = data_set.order()
        message = data_set.show(data_copy)
        self.assertIsInstance(message, str)
        print(message)
        print("#"*25)
        print(output)
        data_set.addChild(data_e)
        self.assertIn('DataE', data_set.data)


if __name__ == '__main__':
    unittest.main()
