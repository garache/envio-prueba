from typing import List
from input import data


priority = ('Highest', 'High', 'Medium', 'Low', 'Lowest',)
levels = ('Zero', 'One', 'Two', 'Three', 'Four',)


class DataSet:
    data_order: dict = None
    ordenaciones_padre: List[str] = []
    ordenaciones_hijo: List[str] = []
    ordenaciones_nieto: List[str] = []

    def __init__(self, data: dict):
        self.data = data

    def addChild(self, child: dict):
        self.data.update(child)

    '''
    Es esta función tomaremos la variable de clase data, la copiaremos
    y agregaremos un campo order que es la concatenacion del level y priority
    para facilitar la ordenación mas adelante
    '''
    def order(self) -> dict:
        # primero obtenemos los padres
        data_order: dict = self.data.copy()
        for key, value in data_order.items():
            data_order[key]['order'] = \
                f"{levels.index(data_order[key]['level'])},{priority.index(data_order[key]['priority'])}"
            self.ordenaciones_padre.append(data_order[key]['order'])
            for k, v in value.items():
                # los hijos son instancias de dict
                if isinstance(v, dict):
                    v['order'] = \
                        f"{levels.index(v['level'])},{priority.index(v['priority'])}"
                    self.ordenaciones_hijo.append(v['order'])
                    # verficamos en caso de que el hijo tenga hijos tercer nivel
                    for ke, va in v.items():
                        if isinstance(va, dict):
                            va['order'] = \
                                f"{levels.index(va['level'])},{priority.index(va['priority'])}"
                            self.ordenaciones_nieto.append(va['order'])

        return data_order

    def show(self, data: dict):
        new_data = data.copy()
        message: str = ''
        # primero obtenemos los padres:
        message = f"{'-'*12}Data{'-'*12}\n"
        message += '\n'

        anadidos: List[str] = []
        for ordenacion in sorted(self.ordenaciones_padre):
            for key, value in new_data.items():
                if new_data[key]['order'] == ordenacion:
                    message += f"> {new_data[key]['name']}"
                    message += "\n"
                    # Mostramos los hijos que contiene el padre
                    # for ordenacion_hijo in sorted(self.ordenaciones_hijo):
                    for ordenacion_hijo in sorted(self.ordenaciones_hijo):
                        for _, va in value.items():
                            if isinstance(va, dict):
                                if va['order'] == ordenacion_hijo:
                                    # Evitamos imprimir datos que ya forma parte del mensaje
                                    if not va['name'] in anadidos:
                                        message += f"-----> {va['name']}"
                                        message +="\n"
                                        anadidos.append(va['name'])
                                        # Si hay un tercer nivel de anidamiento, buscamos los nietis
                                        # los hijos de los hijos
                                        for ordenacion_nieto in sorted(self.ordenaciones_nieto):
                                            for _, v in va.items():
                                                if isinstance(v, dict):
                                                    if v['order'] == ordenacion_nieto:
                                                        message += f"----------> {v['name']}"
                                                        message +="\n" 
        message += '\n'
        message += f"{'-'*12}Data{'-'*12}\n"
        return message


def main():
    # todo: code here

    data_set = DataSet(data)
    data_e = {
        "DataE": {
            "name":"One nameE",
            "level": "Four",
            "priority": "Lowest",
        }
    }
    # Agregamos un nodo mas al data inicial
    data_set.addChild(data_e)
    data_copy = data_set.order()
    message = data_set.show(data_copy)
    print(message)


if __name__ == '__main__':
    main()
