## Mejoras a implementar en ORM

Crear una estructura en capas (carpetas)
- Archivo models.py
- Archivo services.py (archivo que contendrá la lógica del negocio, es decir las consultas a la bd).

Manejo de un contendor con volumen, para facilitar el despliegue

Modularidad de código, crear funciones que verifiquen la disponibilidad del vehiculó, para validar los datos obligatorios, cada una por tarea concreta, etc.

En la tabla asignaciones agregar una campo de tipo date para guardar la fecha de inicio de esta manera los vehículos asignados se pueden manejar para el chofer por rango de fecha y no estar asignando día a día el mismo vehículo.

  

En cada tabla agregar un campo de tipo boolean activo de esta manera aremos un borrado lógico de registros que ya no ocupemos, pero que sean necesarios para un histórico.

  

Aplicar herencia múltiple a los modelos, crear una clase genérica para tareas comunes a los modelos por ejemplo el CRUD.

  

Agregar un campo para saber quien registro una asignación de vehículo, pensando en auditorias.

  

Con relación al punto anterior también seria necesario agregar una tabla de usuarios con campos usuario id, usuario entre otros.